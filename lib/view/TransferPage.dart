import 'package:easy_gradient_text/easy_gradient_text.dart';
import 'package:flutter/material.dart';
import 'package:mockva/view/Components/Button/NormalRaisedButton.dart';
import 'package:mockva/view/Components/TextFormField/NormalTextFormField.dart';

import 'theme/AppColor.dart';

class TransferPage extends StatefulWidget {
  @override
  _TransferPageState createState() => _TransferPageState();
}

class _TransferPageState extends State<TransferPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        shadowColor: null,
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: GradientText(
          text: "TRANSFER",
          gradientDirection: GradientDirection.btt,
          colors: [AppColor.primary, AppColor.secondary],
          style:
              TextStyle(fontSize: 30, fontWeight: FontWeight.bold, height: 1),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.fromLTRB(
                    30, MediaQuery.of(context).size.height * 0.05, 30, 0),
                width: MediaQuery.of(context).size.width,
                height: 170,
                padding: EdgeInsets.all(20),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  gradient: LinearGradient(
                      begin: Alignment.bottomCenter,
                      end: Alignment.topCenter,
                      colors: [AppColor.primary, AppColor.secondary]),
                  boxShadow: [
                    new BoxShadow(
                      color: Colors.black.withOpacity(0.05),
                      blurRadius: 20.0,
                    ),
                  ],
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Balance",
                                  style: TextStyle(
                                      height: 1,
                                      color: Colors.white,
                                      fontWeight: FontWeight.w600,
                                      fontSize: 20)),
                              Text(
                                "IDR 3.000.000",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 30,
                                    fontWeight: FontWeight.bold,
                                    height: 1),
                              )
                            ],
                          ),
                        ),
                        Text(
                          "MOCKVA",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 35,
                              fontWeight: FontWeight.bold,
                              height: 1),
                        )
                      ],
                    ),
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "848552185556625",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 30,
                                fontWeight: FontWeight.bold,
                                height: 1),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height * 0.05),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 30),
                child: Column(
                  children: [
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Account Destination",
                              style: TextStyle(
                                  height: 1,
                                  color: AppColor.semiDarkGrey,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 20)),
                          NormalTextFormField(
                            hint: "Input Account Destination",
                          )
                        ],
                      ),
                    ),
                    SizedBox(height: 20),
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Amount",
                              style: TextStyle(
                                  height: 1,
                                  color: AppColor.semiDarkGrey,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 20)),
                          NormalTextFormField(
                            hint: "Input Amount",
                          )
                        ],
                      ),
                    ),
                    SizedBox(height: 50),
                    NormalRaisedButton(
                      onPressed: () {},
                      color: AppColor.primary,
                      text: "Transfer",
                      padding: EdgeInsets.symmetric(vertical: 10),
                      elevation: 0.5,
                    ),
                    SizedBox(height: 50)
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
