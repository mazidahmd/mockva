import 'package:easy_gradient_text/easy_gradient_text.dart';
import 'package:flutter/material.dart';
import 'package:mockva/view/Components/Button/NormalRaisedButton.dart';
import 'package:mockva/view/Components/TextFormField/NormalTextFormField.dart';

import 'theme/AppColor.dart';

class AccountPage extends StatefulWidget {
  @override
  _AccountPageState createState() => _AccountPageState();
}

class _AccountPageState extends State<AccountPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        shadowColor: null,
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: GradientText(
          text: "ACCOUNT",
          gradientDirection: GradientDirection.btt,
          colors: [AppColor.primary, AppColor.secondary],
          style:
              TextStyle(fontSize: 30, fontWeight: FontWeight.bold, height: 1),
        ),
      ),
      body: Center(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 50),
          child: NormalRaisedButton(
            onPressed: () {},
            color: AppColor.primary,
            elevation: 0,
            padding: EdgeInsets.symmetric(vertical: 15),
            text: "LOG OUT",
          ),
        ),
      ),
    );
  }
}
