import 'package:flutter/material.dart';
import 'package:mockva/view/theme/AppColor.dart';

class AppTheme {
  static ThemeData get light => ThemeData(
      colorScheme: ColorScheme.light(
          primary: AppColor.primary,
          onPrimary: AppColor.onPrimary,
          secondary: AppColor.secondary,
          background: AppColor.background,
          error: AppColor.error,
          onError: AppColor.onError),
      fontFamily: 'PFHandbookPro');
}
