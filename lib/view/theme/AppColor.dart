import 'package:flutter/material.dart';

class AppColor {
  static Color get primary => Color(0xFF16BABD);
  static Color get onPrimary => Colors.white;
  static Color get secondary => Color(0xFF2574C5);
  static Color get error => Color(0xFFFF3244);
  static Color get onError => Colors.white;
  static Color get background => Color(0XFFFCFCFC);
  static Color get onBackground => Color(0xFF454545);
  static Color get darkGrey => Color(0xFF454545);
  static Color get semiDarkGrey => Color(0xFF828282);
  static Color get ceramicWhite => Color(0xFF454545);
}
