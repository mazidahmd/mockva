import 'package:easy_gradient_text/easy_gradient_text.dart';
import 'package:flutter/material.dart';
import 'package:mockva/view/theme/AppAssetImage.dart';
import 'package:mockva/view/theme/AppColor.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      child: Stack(children: [
        Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height / 2,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [AppColor.primary, AppColor.secondary])),
          child: Center(
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle, color: Colors.white),
                    child: Image(
                      image: AppAssetImage.daksaLogo,
                      width: 60,
                    ),
                  ),
                  Text(
                    "MOCKVA",
                    style: TextStyle(
                        fontSize: 40,
                        fontWeight: FontWeight.bold,
                        height: 1,
                        color: Colors.white),
                  ),
                  Text(
                    "Mobile",
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        height: 1,
                        color: Colors.white),
                  ),
                ],
              ),
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(
              30, MediaQuery.of(context).size.height / 2.5, 30, 0),
          width: MediaQuery.of(context).size.width,
          height: 210,
          padding: EdgeInsets.all(20),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
            boxShadow: [
              new BoxShadow(
                color: Colors.black.withOpacity(0.05),
                blurRadius: 20.0,
              ),
            ],
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Name",
                            style: TextStyle(
                                height: 1,
                                color: AppColor.semiDarkGrey,
                                fontWeight: FontWeight.w600,
                                fontSize: 20)),
                        GradientText(
                          text: "Honey Wesley",
                          gradientDirection: GradientDirection.btt,
                          colors: [AppColor.primary, AppColor.secondary],
                          style: TextStyle(
                              fontSize: 30,
                              fontWeight: FontWeight.bold,
                              height: 1),
                        )
                      ],
                    ),
                  ),
                  GradientText(
                    text: "MOCKVA",
                    gradientDirection: GradientDirection.btt,
                    colors: [AppColor.primary, AppColor.secondary],
                    style: TextStyle(
                        fontSize: 35, fontWeight: FontWeight.bold, height: 1),
                  )
                ],
              ),
              SizedBox(height: 10),
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Account Number",
                        style: TextStyle(
                            height: 1,
                            color: AppColor.semiDarkGrey,
                            fontWeight: FontWeight.w600,
                            fontSize: 20)),
                    GradientText(
                      text: "814848551056164",
                      gradientDirection: GradientDirection.btt,
                      colors: [AppColor.primary, AppColor.secondary],
                      style: TextStyle(
                          fontSize: 30, fontWeight: FontWeight.bold, height: 1),
                    )
                  ],
                ),
              ),
              SizedBox(height: 10),
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Balance",
                        style: TextStyle(
                            height: 1,
                            color: AppColor.semiDarkGrey,
                            fontWeight: FontWeight.w600,
                            fontSize: 20)),
                    GradientText(
                      text: "IDR 3.000.000",
                      gradientDirection: GradientDirection.btt,
                      colors: [AppColor.primary, AppColor.secondary],
                      style: TextStyle(
                          fontSize: 30, fontWeight: FontWeight.bold, height: 1),
                    )
                  ],
                ),
              ),
            ],
          ),
        )
      ]),
    ));
  }
}
