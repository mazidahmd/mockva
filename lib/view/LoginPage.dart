import 'package:easy_gradient_text/easy_gradient_text.dart';
import 'package:flutter/material.dart';
import 'package:mockva/view/theme/AppAssetImage.dart';
import 'package:mockva/view/theme/AppColor.dart';

import 'Components/Button/NormalRaisedButton.dart';
import 'Components/TextFormField/IconButtonTextFormField.dart';
import 'Components/TextFormField/NormalTextFormField.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: Container(
        alignment: Alignment.center,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image(
                image: AppAssetImage.daksaLogo,
                width: 100,
              ),
              GradientText(
                text: "MOCKVA",
                gradientDirection: GradientDirection.btt,
                colors: [AppColor.primary, AppColor.secondary],
                style: TextStyle(
                    fontSize: 80, fontWeight: FontWeight.bold, height: 1),
              ),
              Text(
                "Mobile",
                style: TextStyle(
                    fontSize: 40,
                    height: 1,
                    fontWeight: FontWeight.bold,
                    color: AppColor.darkGrey),
              ),
              SizedBox(height: 20),
              Form(
                child: Column(
                  children: [
                    NormalTextFormField(
                      label: 'Email',
                      hint: 'Masukkan email',
                      type: TextInputType.emailAddress,
                      prefixIcon: Icons.email_outlined,
                      validator: (String value) {
                        if (value.isEmpty) {
                          return 'Email tidak boleh kosong';
                        }
                        return null;
                      },
                    ),
                    IconButtonTextFormField(
                      label: 'Password',
                      hint: 'Masukkan password',
                      type: TextInputType.visiblePassword,
                      prefixIcon: Icons.lock_outline,
                      validator: (String value) {
                        if (value.isEmpty) {
                          return 'Password tidak boleh kosong';
                        }
                        return null;
                      },
                      buttonOnPressed: () {},
                    ),
                    SizedBox(height: 40),
                    NormalRaisedButton(
                      width: 300,
                      text: 'Login',
                      elevation: 0,
                      padding: EdgeInsets.symmetric(
                        horizontal: 20,
                        vertical: 15,
                      ),
                      onPressed: () {},
                    ),
                    SizedBox(height: 40),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    ));
  }
}
