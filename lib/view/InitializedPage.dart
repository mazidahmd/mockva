import 'package:easy_gradient_text/easy_gradient_text.dart';
import 'package:flutter/material.dart';
import 'package:mockva/view/theme/AppAssetImage.dart';
import 'package:mockva/view/theme/AppColor.dart';

class InitializedPage extends StatefulWidget {
  @override
  _InitializedPageState createState() => _InitializedPageState();
}

class _InitializedPageState extends State<InitializedPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image(
            image: AppAssetImage.daksaLogo,
            width: 100,
          ),
          GradientText(
            text: "MOCKVA",
            gradientDirection: GradientDirection.btt,
            colors: [AppColor.primary, AppColor.secondary],
            style:
                TextStyle(fontSize: 80, fontWeight: FontWeight.bold, height: 1),
          ),
          Text(
            "Mobile",
            style: TextStyle(
                fontSize: 40,
                fontWeight: FontWeight.bold,
                color: AppColor.darkGrey),
          )
        ],
      )),
    );
  }
}
