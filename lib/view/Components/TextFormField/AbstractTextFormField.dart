import 'package:flutter/material.dart';

abstract class AbstractTextFormField extends StatelessWidget {
  final Key key;
  final String label;
  final String hint;
  final TextInputType type;
  final bool obscureText;
  final TextEditingController controller;
  final double width;

  AbstractTextFormField({this.key, this.label, this.hint, this.type, this.obscureText, this.controller, this.width});

  @override
  Widget build(BuildContext context);
}
