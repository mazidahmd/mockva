import 'package:flutter/material.dart';

import 'AbstractTextFormField.dart';
import 'IconButtonTextFormField.dart';

class NormalTextFormField extends AbstractTextFormField {
  final IconData prefixIcon;
  final width;
  final String Function(String) validator;
  final void Function(String) onChanged;

  NormalTextFormField({
    Key key,
    String label,
    String hint,
    TextInputType type,
    bool obscureText,
    TextEditingController controller,
    this.validator,
    this.width,
    this.prefixIcon,
    this.onChanged
  }) : super(
          key: key,
          label: label,
          hint: hint,
          type: type,
          obscureText: obscureText,
          controller: controller
        );

  @override
  Widget build(BuildContext context) {
    return IconButtonTextFormField(
      key: key,
      label: label,
      hint: hint,
      type: type,
      prefixIcon: prefixIcon,
      obscureText: obscureText ?? false,
      width: width ?? null,
      controller: controller,
      validator: validator ?? null,
      onChanged: onChanged ?? null,
    );
  }
}
