import 'package:flutter/material.dart';

import 'AbstractTextFormField.dart';
import 'IconButtonTextFormField.dart';

class RegisterTextFormField extends AbstractTextFormField {
  final String label;
  final String Function(String) validator;
  String filledText;

  RegisterTextFormField({
    Key key,
    this.label,
    this.filledText,
    this.validator,
    TextInputType type,
    bool obscureText,
    TextEditingController controller,
  }) : super(
            key: key,
            label: label != null ? label.replaceAll("* ", "") : null,
            type: type,
            obscureText: obscureText,
            controller: controller);

  @override
  Widget build(BuildContext context) {
    return IconButtonTextFormField(
      label: label,
      type: type,
      obscureText: obscureText,
      controller: controller,
      verticalMargin: 5.0,
      suffixText: label != null
          ? label.contains('*')
              ? '*'
              : null
          : null,
      suffixStyle: TextStyle(
        color: Theme.of(context).colorScheme.error,
      ),
      hint: label != null ? "Masukkan " + label.replaceAll("* ", "") : null,
      filledText: filledText,
      validator: validator,
    );
  }
}
