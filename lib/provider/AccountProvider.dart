import 'dart:convert';
import 'package:http/http.dart';
import 'package:mockva/model/Account.dart';
import 'package:mockva/util/ApiHandler.dart';

class AccountProvider {
  Client client = Client();
  final _baseUrl = ApiHandler().apiUrl;

  Future<Account> getDetail() async {
    final _url = Uri.parse("$_baseUrl/account/detail");
    final response = await client
        .get(_url, headers: {"_sessionId": ApiHandler().session.id});

    final responseJson = jsonDecode(response.body);
    if (response.statusCode == 200) {
      return Account.fromJson(responseJson);
    } else {
      return null;
    }
  }
}
