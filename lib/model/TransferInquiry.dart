import 'dart:convert';

class TransferInquiry {
  String inquiryId;
  String accountSrcId;
  String accountDstId;
  String accountSrcName;
  String accountDstName;
  int amount;
  TransferInquiry({
    this.inquiryId,
    this.accountSrcId,
    this.accountDstId,
    this.accountSrcName,
    this.accountDstName,
    this.amount,
  });

  Map<String, dynamic> toMap() {
    return {
      'inquiryId': inquiryId,
      'accountSrcId': accountSrcId,
      'accountDstId': accountDstId,
      'accountSrcName': accountSrcName,
      'accountDstName': accountDstName,
      'amount': amount,
    };
  }

  Map<String, dynamic> toMapTransfer() {
    return {
      'accountSrcId': accountSrcId,
      'accountDstId': accountDstId,
      'amount': amount,
    };
  }

  factory TransferInquiry.fromMap(Map<String, dynamic> map) {
    return TransferInquiry(
      inquiryId: map['inquiryId'],
      accountSrcId: map['accountSrcId'],
      accountDstId: map['accountDstId'],
      accountSrcName: map['accountSrcName'],
      accountDstName: map['accountDstName'],
      amount: map['amount'],
    );
  }

  String toJson() => json.encode(toMap());
  String toJsonTransfer() => json.encode(toMapTransfer());

  factory TransferInquiry.fromJson(String source) =>
      TransferInquiry.fromMap(json.decode(source));

  @override
  String toString() {
    return 'TransferInquiry(inquiryId: $inquiryId, accountSrcId: $accountSrcId, accountDstId: $accountDstId, accountSrcName: $accountSrcName, accountDstName: $accountDstName, amount: $amount)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is TransferInquiry &&
        other.inquiryId == inquiryId &&
        other.accountSrcId == accountSrcId &&
        other.accountDstId == accountDstId &&
        other.accountSrcName == accountSrcName &&
        other.accountDstName == accountDstName &&
        other.amount == amount;
  }

  @override
  int get hashCode {
    return inquiryId.hashCode ^
        accountSrcId.hashCode ^
        accountDstId.hashCode ^
        accountSrcName.hashCode ^
        accountDstName.hashCode ^
        amount.hashCode;
  }
}
