import 'dart:convert';

class Transaction {
  String id;
  String accountSrcId;
  String accountDstId;
  String clientRef;
  int amount;
  DateTime transactionTimeStamp;
  Transaction({
    this.id,
    this.accountSrcId,
    this.accountDstId,
    this.clientRef,
    this.amount,
    this.transactionTimeStamp,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'accountSrcId': accountSrcId,
      'accountDstId': accountDstId,
      'clientRef': clientRef,
      'amount': amount,
      'transactionTimeStamp': transactionTimeStamp.millisecondsSinceEpoch,
    };
  }

  factory Transaction.fromMap(Map<String, dynamic> map) {
    return Transaction(
      id: map['id'],
      accountSrcId: map['accountSrcId'],
      accountDstId: map['accountDstId'],
      clientRef: map['clientRef'],
      amount: map['amount'],
      transactionTimeStamp:
          DateTime.fromMillisecondsSinceEpoch(map['transactionTimeStamp']),
    );
  }

  String toJson() => json.encode(toMap());

  factory Transaction.fromJson(String source) =>
      Transaction.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Transaction(id: $id, accountSrcId: $accountSrcId, accountDstId: $accountDstId, clientRef: $clientRef, amount: $amount, transactionTimeStamp: $transactionTimeStamp)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Transaction &&
        other.id == id &&
        other.accountSrcId == accountSrcId &&
        other.accountDstId == accountDstId &&
        other.clientRef == clientRef &&
        other.amount == amount &&
        other.transactionTimeStamp == transactionTimeStamp;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        accountSrcId.hashCode ^
        accountDstId.hashCode ^
        clientRef.hashCode ^
        amount.hashCode ^
        transactionTimeStamp.hashCode;
  }
}
