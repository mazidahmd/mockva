import 'dart:convert';

import 'package:flutter/foundation.dart';

import 'package:mockva/model/Transaction.dart';

class Transactions {
  List<Transaction> data;
  Transactions({
    this.data,
  });

  Map<String, dynamic> toMap() {
    return {
      'data': data?.map((x) => x.toMap())?.toList(),
    };
  }

  factory Transactions.fromMap(Map<String, dynamic> map) {
    return Transactions(
      data: List<Transaction>.from(
          map['data']?.map((x) => Transaction.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory Transactions.fromJson(String source) =>
      Transactions.fromMap(json.decode(source));

  @override
  String toString() => 'Transactions(data: $data)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Transactions && listEquals(other.data, data);
  }

  @override
  int get hashCode => data.hashCode;
}
