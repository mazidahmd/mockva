import 'package:mockva/model/Account.dart';
import 'package:mockva/provider/AccountProvider.dart';

class AccountRepository {
  final AccountProvider _provider = new AccountProvider();

  Future<Account> getDetail() async {
    return await _provider.getDetail();
  }
}
